ARG GO_VERSION=1.12.5
ARG ALPINE_VERSION=3.9

FROM golang:${GO_VERSION}-alpine${ALPINE_VERSION} as build

WORKDIR /usr/src/app

COPY . ./
RUN apk add --no-cache git jq
RUN go get github.com/fzipp/gocyclo
RUN export gocyclo_version=$(grep fzipp/gocyclo go.mod | cut -d ' ' -f 2) && \
    export version=$(git describe --tag --always) && \
    env | grep version && \
    cat engine.json | jq '.version = env.gocyclo_version + "-" + env.version' > ./engine.json

RUN go get -t -d -v .
RUN go build -o codeclimate-gocyclo .


FROM alpine:${ALPINE_VERSION}
LABEL maintainer="Alessio Caiazza"

RUN adduser -u 9000 -D app

WORKDIR /code

COPY --from=build /usr/src/app/engine.json /
COPY --from=build /usr/src/app/codeclimate-gocyclo /usr/bin/codeclimate-gocyclo
COPY --from=build /go/bin/gocyclo /usr/bin/gocyclo

USER app

VOLUME /code

CMD ["/usr/bin/codeclimate-gocyclo"]

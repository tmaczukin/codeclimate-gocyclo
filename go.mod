module gitlab.com/nolith/codeclimate-gocyclo

go 1.12

require (
	github.com/codeclimate/cc-engine-go v0.0.0-20160217183426-55f9c825e2a9
	github.com/fzipp/gocyclo v0.0.0-20150627053110-6acd4345c835 // indirect
)
